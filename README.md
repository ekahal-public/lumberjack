
# LumberJack

A simple and easiest way to implement logging mechanism to your Laravel application.

## Installation

`composer require ekahal/lumberjack`

### Add Environment Variables (.env)
```
MONGODB_HOST=cluster0.abcde.mongodb.net
MONGODB_DATABASE=database
MONGODB_USERNAME=username
MONGODB_PASSWORD=passsword
LUMBERJACK_SECRET=secretcode
```

### Add Database Driver to Config (config/database.php)
```php
'connections' => [
    ....
    'ekljmongodb' => [
        'driver' => 'mongodb',
        'dsn' => 'mongodb+srv://' . env('MONGODB_USERNAME') . ':' . env('MONGODB_PASSWORD') . '@' . env('MONGODB_HOST') . '/' . env('MONGODB_DATABASE') . '?retryWrites=true&w=majority',
        'database' => env('MONGODB_DATABASE', 'myappdb'),
    ],
    ....
],
```

### Publish the Config File

```
php artisan vendor:publish --provider="Ekahal\LumberJack\ServiceProvider"
```

## Usage

### Via Laravel

```php
use Ekahal\LumberJack\LumberJack;

...
LumberJack::log(USER_ID, URL, ACTION);
LumberJack::track(USER_ID, URL, CUSTOM_ACTION, IP, LOCATION, BROWSER, DEVICE);
...
```

### Via Javascript (Vanilla, Vue, React, etc)

You will need to pass the secret as the first parameter which you have set earlier in the environment variables for Laravel.

```js
lumberjack.log(SECRET, USER_ID, URL, ACTION);
lumberjack.track(SECRET, USER_ID, URL, CUSTOM_ACTION, IP, LOCATION, BROWSER, DEVICE);
```

### Recommended ACTION Variables

C - Create
R - Read
U - Update
D - Delete

## Fetch Logs

You can use the LumberJack inbuilt function to fetch records in your Admin Panel or similar platform.

```php
use Ekahal\LumberJack\LumberJack;

...
$filters = [
    'user_id' => USER_ID,
    'url' => URL,
    'action' => ACTION,
    'ip' => IP,
    'location' => LOCATION,
    'browser' => BROWSER,
    'device' => DEVICE,
    'from' => DATE_FROM,
    'to' => DATE_TO
];
$logs = LumberJack::fetch($filters);
...
```