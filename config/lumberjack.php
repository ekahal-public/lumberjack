<?php

return [
    /**
     * -----------------------------------------------------------
     * LumberJack Secret
     * -----------------------------------------------------------
     * 
     * Secret is used to validate any Javascript calls to the API.
     * It is passed as the first parameter to javascript function
     * call. Eg. lumberjack.log(SECRET, ...) 
     */

    'secret' => env('LUMBERJACK_SECRET')
];
