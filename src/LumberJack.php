<?php

namespace Ekahal\LumberJack;

use Carbon\Carbon;
use Ekahal\LumberJack\Models\LumberJack as ModelsLumberJack;

class LumberJack {

    public static function log($user_id, $url, $action) {
        $log = new ModelsLumberJack;
        $log->user_id = $user_id;
        $log->url = $url;
        $log->action = $action;
        $log->date = Carbon::now();
        $log->save();
    }

    public static function track($user_id, $url, $action, $ip = '', $location = '', $browser = '', $device = '') {
        $log = new ModelsLumberJack;
        $log->user_id = $user_id;
        $log->url = $url;
        $log->action = $action;
        $log->ip = $ip;
        $log->location = $location;
        $log->browser = $browser;
        $log->device = $device;
        $log->date = Carbon::now();
        $log->save();
    }

    public static function fetch($filters = []) {
        $query = ModelsLumberJack::query();
        if (isset($filters['user_id'])) {
            $query->where('user_id', $filters['user_id']);
        }
        if (isset($filters['url'])) {
            $query->where('url', $filters['url']);
        }
        if (isset($filters['action'])) {
            $query->where('action', $filters['action']);
        }
        if (isset($filters['ip'])) {
            $query->where('ip', $filters['ip']);
        }
        if (isset($filters['location'])) {
            $query->where('location', $filters['location']);
        }
        if (isset($filters['browser'])) {
            $query->where('browser', $filters['browser']);
        }
        if (isset($filters['device'])) {
            $query->where('device', $filters['device']);
        }
        if (isset($filters['from'])) {
            $query->where('date', '>', $filters['from']);
        }
        if (isset($filters['to'])) {
            $query->where('date', '<', $filters['to']);
        }
        return $query->get();
    }
}
