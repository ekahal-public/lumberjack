<?php

namespace Ekahal\LumberJack\Middleware;

use Closure;

class InjectLumberJack {

    /**
     * This middleware injects Javascript functions to be utilized on the frontend of the application
     */
    public function handle($request, Closure $next) {
        $js = '
        <script>
        let lumberjack = {}
        lumberjack.log = function (secret, user_id, url, action) {
            if(!document.querySelector(`meta[name="csrf-token"]`)) {
                console.error("Cannot find CSRF Token. Please make sure you\'ve meta tag containing the CSRF token.")
            } else {
                let _token = document.querySelector(`meta[name="csrf-token"]`).getAttribute("content");
                fetch(`/lumberjack/log`, {
                    method: `POST`,
                    headers: {
                        accept: `application.json`,
                        "Content-Type": `application/json`
                    },
                    body: JSON.stringify({ secret, user_id, url, action, _token })
                })
            }
        }
        lumberjack.track = function (secret, user_id, url, action, ip = "", location = "", browser = "", device = "") {
            if(!document.querySelector(`meta[name="csrf-token"]`)) {
                console.error("Cannot find CSRF Token. Please make sure you\'ve meta tag containing the CSRF token.")
            } else {
                let _token = document.querySelector(`meta[name="csrf-token"]`).getAttribute("content");
                fetch(`/lumberjack/track`, {
                    method: `POST`,
                    headers: {
                        accept: `application.json`,
                        "Content-Type": `application/json`
                    },
                    body: JSON.stringify({ secret, user_id, url, action, ip, location, browser, device, _token })
                })
            }
        }
        </script>
        ';
        $response = $next($request);
        $content = $response->getContent();

        // Try to put the js directly before the </head>
        $pos = strripos($content, '</head>');
        if (false !== $pos) {
            $content = substr($content, 0, $pos) . $js . substr($content, $pos);
            $response->setContent($content);
        }

        $response->headers->remove('Content-Length');

        return $response;
    }
}
