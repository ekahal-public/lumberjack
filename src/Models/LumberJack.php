<?php

namespace Ekahal\LumberJack\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class LumberJack extends Model {

    protected $connection = 'ekljmongodb';
    protected $table = 'lumberjack';
    protected $dates = ['date'];
    public $timestamps = false;
}
