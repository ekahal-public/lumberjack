<?php

use Ekahal\LumberJack\LumberJack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('lumberjack/log', function (Request $request) {
    if ($request->secret && $request->secret == config('lumberjack.secret')) {
        LumberJack::log($request->user_id, $request->url, $request->action);
        return response('OK', 200);
    }
    return response('FAILED', 403);
});

Route::post('lumberjack/track', function (Request $request) {
    if ($request->secret && $request->secret == config('lumberjack.secret')) {
        LumberJack::track($request->user_id, $request->url, $request->action, $request->ip, $request->location, $request->browser, $request->device);
        return response('OK', 200);
    }
    return response('FAILED', 403);
});
