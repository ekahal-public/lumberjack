<?php

namespace Ekahal\LumberJack;

use Ekahal\LumberJack\Middleware\InjectLumberJack;
use Illuminate\Contracts\Http\Kernel;

class ServiceProvider extends \Illuminate\Support\ServiceProvider {

    public function boot() {
        $this->publishes([
            __DIR__ . '/../config/lumberjack.php' => config_path('lumberjack.php'),
        ]);
        $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');
        $this->registerMiddleware(InjectLumberJack::class);
    }

    protected function registerMiddleware($middleware) {
        $kernel = $this->app[Kernel::class];
        $kernel->pushMiddleware($middleware);
    }
}
